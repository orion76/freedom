<?php

namespace Drupal\entity_event\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Entity action plugins.
 */
interface EntityActionInterface extends PluginInspectionInterface {


  // Add get/set methods for your plugin type here.

}
