<?php

namespace Drupal\entity_event\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Entity event plugins.
 */
interface EntityEventInterface extends PluginInspectionInterface {


  // Add get/set methods for your plugin type here.

}
