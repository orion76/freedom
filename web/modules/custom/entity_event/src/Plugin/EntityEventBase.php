<?php

namespace Drupal\entity_event\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Entity event plugins.
 */
abstract class EntityEventBase extends PluginBase implements EntityEventInterface {


  // Add common methods and abstract methods for your plugin type here.

}
