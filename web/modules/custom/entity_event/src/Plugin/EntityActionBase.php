<?php

namespace Drupal\entity_event\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Entity action plugins.
 */
abstract class EntityActionBase extends PluginBase implements EntityActionInterface {


  // Add common methods and abstract methods for your plugin type here.

}
