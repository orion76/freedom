<?php

namespace Drupal\entity_event\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Config group entities.
 */
interface ConfigGroupInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
