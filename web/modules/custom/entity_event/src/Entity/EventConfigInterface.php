<?php

namespace Drupal\entity_event\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Event config entities.
 */
interface EventConfigInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
